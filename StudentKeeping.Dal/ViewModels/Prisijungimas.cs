﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentKeeping.Dal.ViewModels
{
    public class Prisijungimas
    {
        public string NaudotojoVardas { get; set; }

        public string Slaptažodis { get; set; }

        public PrisijungimoStatusas? PrisijungimoStatusas { get; set; }

        public Sesija Sesija { get; set; }
    }
}
