﻿using MySql.Data.MySqlClient;
using StudentKeeping.Dal;
using StudentKeeping.Dal.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentKeeping.Services
{
    public class LoginService : DBConnection
    {
        public LoginService() { }


        public Prisijungimas Prisijungti(Prisijungimas prisijungimas)
        {
            var loginSql = "Prisijungti";

            var loginCmd = new MySqlCommand(loginSql, Connection);
            loginCmd.CommandType = System.Data.CommandType.StoredProcedure;

            loginCmd.Parameters.AddWithValue("InNaudotojoVardas", prisijungimas.NaudotojoVardas);
            loginCmd.Parameters.AddWithValue("InSlaptazodis", prisijungimas.Slaptažodis);
            loginCmd.Parameters.Add(new MySqlParameter("PrisijungimoStatusas", MySqlDbType.Int32));
            loginCmd.Parameters.Add(new MySqlParameter("OutVardas", MySqlDbType.VarChar));
            loginCmd.Parameters.Add(new MySqlParameter("OutPavarde", MySqlDbType.VarChar));
            loginCmd.Parameters.Add(new MySqlParameter("OutRole", MySqlDbType.Int32));
            loginCmd.Parameters.Add(new MySqlParameter("OutNaudotojoId", MySqlDbType.Int32));

            loginCmd.Parameters["InNaudotojoVardas"].Direction = System.Data.ParameterDirection.Input;
            loginCmd.Parameters["InSlaptazodis"].Direction = System.Data.ParameterDirection.Input;
            loginCmd.Parameters["PrisijungimoStatusas"].Direction = System.Data.ParameterDirection.Output;
            loginCmd.Parameters["OutVardas"].Direction = System.Data.ParameterDirection.Output;
            loginCmd.Parameters["OutPavarde"].Direction = System.Data.ParameterDirection.Output;
            loginCmd.Parameters["OutRole"].Direction = System.Data.ParameterDirection.Output;
            loginCmd.Parameters["OutNaudotojoId"].Direction = System.Data.ParameterDirection.Output;

            try
            {
                Connection.Open();
                var result = loginCmd.ExecuteNonQuery();
                var statusas = (PrisijungimoStatusas)loginCmd.Parameters["PrisijungimoStatusas"].Value;

                prisijungimas.PrisijungimoStatusas = statusas;

                if (prisijungimas.PrisijungimoStatusas == PrisijungimoStatusas.Prisijugta)
                {
                    prisijungimas.Sesija = new Sesija
                    {
                        NaudotojoVardas = prisijungimas.NaudotojoVardas,
                        Vardas = loginCmd.Parameters["OutVardas"].Value.ToString(),
                        Pavarde = loginCmd.Parameters["OutPavarde"].Value.ToString(),
                        Role = (Role)loginCmd.Parameters["OutRole"].Value,
                        NaudotojoId = (int)loginCmd.Parameters["OutNaudotojoId"].Value
                    };
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Connection.Close();
            }
            return prisijungimas;
        }
    }
}
